/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* 
Copyright 2016 iGenie Ltd
*/ 
package com.kislay.parse.DTD;

import com.kislay.matra.decl.ElementType;
import com.kislay.matra.dtdparser.DTDParser;
import com.kislay.matra.exception.DTDException;
import com.kislay.matra.io.DTDFile;
import com.kislay.matra.io.DTDSource;
import java.io.File;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author kislay
 */
public class GetNodes {

    public static List getList(String file, String root) throws DTDException {
        DTDParser parser = new DTDParser();
        DTDSource source = new DTDFile(file);
        parser.parse(source);
        Hashtable hs = parser.getDocType().getElementList();
        Enumeration enu = hs.elements();
        ElementType et = (ElementType) hs.get(root);
        String list[] = et.getChildrenNames();
        List returnList = new ArrayList();
        for (String str : list) {
            returnList.add(str);

        }
        return returnList;
    }

    public static void main(String args[]) throws DTDException {
       List list= getList("/home/kislay/Documents/xml/book.dtd", "book");
       Iterator ite=list.iterator();
       while(ite.hasNext()){
           System.out.println(ite.next());
       }
    }

}
