
/* 
Copyright 2016 iGenie Ltd
*/ 
package com.kislay.mainpack;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import tachyon.client.file.FileInStream;

/**
 *
 * @author kislay
 */
public class MySqlLogger {

    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost/sparklogger";

    //  Database credentials
    static final String USER = "root";
    static final String PASS = "root123";
    static Connection conn = null;
    static Properties prop = new Properties();

    static {
        try {
            prop.load(new FileInputStream("./config.properties"));
            Class.forName(prop.getProperty("dbdriver"));
            conn = DriverManager.getConnection(prop.getProperty("dburl"), prop.getProperty("dbuser"), prop.getProperty("dbpass"));
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(MySqlLogger.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(MySqlLogger.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MySqlLogger.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void logError(String text) {
        try {
            String query = "insert into error_log(time_stamp,station_id,error_text) values(now(),?,?)";
            PreparedStatement prst = conn.prepareStatement(query);
            prst.setString(1, InetAddress.getLocalHost().getHostName());
            prst.setString(2, text);
            prst.execute();
            //
        } catch (Exception exc) {
            System.out.println(exc);

        }
    }

    public static void logInfo(String text) {
        try {
            String query = "insert into info_log(time_stamp,station_id,info_text) values(now(),?,?)";
            PreparedStatement prst = conn.prepareStatement(query);
            prst.setString(1, InetAddress.getLocalHost().getHostName());
            prst.setString(2, text);
            prst.execute();
            //
        } catch (Exception exc) {
            System.out.println(exc);

        }
    }

    public static void main(String args[]) {
        logError("this is test");
    }

}
