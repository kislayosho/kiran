/* 
Copyright 2016 iGenie Ltd
 */

 /* 
Copyright 2016 iGenie Ltd
 */
package com.kislay.mainpack;

import com.kislay.parse.DTD.GetNodes;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.Vector;

import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.spark.Accumulator;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;

public class DataProcessorCustom implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    transient static Logger rootLogger = LogManager.getLogger("myLogger");
    static boolean mysqlenabled = false;

    public static void main(String[] args) throws Exception {
        // Map variables
        // TODO Not checking variables as the same is being done in script

//        String hdfsInputPath = args[0];
//        String fileFormat = args[1];
//        String dtdFilePath = args[2]; // TODO not using as of now
//        String outputPath = args[3];
//        String partitionFormat = args[4];
//        String errorPath = args[5];
//        String schemaPath = args[6];
//        String rowTag = args[7];
//        String validationFile = args[8];
//        mysqlenabled = Boolean.parseBoolean(args[9]);
        String hdfsInputPath = "/home/kislay/Documents/xml/books_failvalidation.xml";
        //String hdfsInputPath = "/home/kislay/Documents/xml/books.xml";
        //String hdfsInputPath = "/home/kislay/Documents/xml/books1.xml";
        String fileFormat = "xml";
        String outputPath = "/home/kislay/Documents/output";
        String partitionFormat = "";
        String rowTag = "book";
        String errorPath = "/home/kislay/Documents/error";
        String dtdFilePath = "/home/kislay/Documents/xml/book.dtd";
        String validationFile = "/home/kislay/Documents/xml/validationfile";
        mysqlenabled = true;

        if (mysqlenabled) {
            MySqlLogger.logInfo("Input: " + hdfsInputPath + " output: " + outputPath + " dtdFilePath: " + dtdFilePath + " rowTag: " + rowTag);
        }

        String formatClass = null;
        Row[] rowsInXml = null;
        Row[] rowsProcessed = null;
        if (mysqlenabled) {
            MySqlLogger.logInfo("dtd: " + dtdFilePath + " rowtag: " + rowTag);
        }

        Vector vecdtd = getQuery(GetNodes.getList(dtdFilePath, rowTag));

        outputPath = outputPath + "/" + partitionFormat;
        errorPath = errorPath + "/" + partitionFormat;

        Logger rootLogger = LogManager.getRootLogger();
        rootLogger.setLevel(Level.INFO);

        SparkConf conf = new SparkConf().setAppName("DataProcessor").setMaster("local[*]");
        JavaSparkContext javaSparkContext = new JavaSparkContext(conf);
        SQLContext sqlContext = new SQLContext(javaSparkContext);
        DataFrame docDF = null;
        final Broadcast<Vector> mapBroadCast = javaSparkContext.broadcast(vecdtd);

        formatClass = "com.databricks.spark.xml";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("rowTag", rowTag);

        docDF = sqlContext.read().format(formatClass).options(params).load(hdfsInputPath);

        docDF.printSchema();
        String cols[] = docDF.columns();

        HashMap map = new HashMap();
        Accumulator<HashMap> mapAccum = javaSparkContext.accumulator(new HashMap(), new HashMapAccumulator());
        Accumulator<HashMap> mapAccumValidator = javaSparkContext.accumulator(new HashMap(), new HashMapAccumulator());
        HashMap localMap = new HashMap();
        String colmysql = "";
        for (String col : cols) {
            rootLogger.info("Column: " + col);
            colmysql = colmysql + col + ",";

            localMap.put(col, "");
        }
        if (mysqlenabled) {
            MySqlLogger.logInfo(" Columns in XML Schema," + colmysql);
        }
        mapAccumValidator.add(getValidation(validationFile));
        mapAccum.add(localMap);

        Vector vec = mapBroadCast.getValue();
        Iterator ite = vec.iterator();
        HashMap mapFromCluster = mapAccum.value();
        HashMap mapFromClusterValidator = mapAccumValidator.value();
        Iterator itevalidator = mapFromClusterValidator.keySet().iterator();
        while (itevalidator.hasNext()) {
            String fieldToValidate = (String) itevalidator.next();
            if (!mapFromCluster.containsKey(fieldToValidate)) {
                if (mysqlenabled) {
                    MySqlLogger.logError("Failed validation on:" + fieldToValidate + " Input file: " + hdfsInputPath + "  Validator File " + validationFile);
                }
                throw new Exception("Failed validation " + fieldToValidate);

            }

        }

        String query = "";

        while (ite.hasNext()) {
            String column = (String) ite.next();
            if (mapFromCluster.containsKey(column)) {
                query = query + column + " as " + column;

            } else {
                query = query + " '' as " + column;
            }
            if (ite.hasNext()) {
                query = query + ",";
            }
        }
        rootLogger.info("query>>" + query);

        rootLogger.info(docDF.schema().simpleString());

        DataFrame assets = docDF;
        assets.registerTempTable(rowTag);

        assets.show();

        DataFrame exSchemaDF;
        try {
            exSchemaDF = sqlContext.sql("select " + query + " from " + rowTag);
            if (mysqlenabled) {
                MySqlLogger.logInfo("Num of rows: " + exSchemaDF.count());
            }
            exSchemaDF.show();
            exSchemaDF.write().format("parquet").mode("overwrite").save(outputPath);
            rootLogger.info("---------- SUCESSFULLY PARSED AND SAVED TO HDFS --------------------");
        } catch (Exception e) {

            rootLogger.info("Saved in Error folder as number of Rows did not match!!!");

        }

    }

    public static HashMap getValidation(String file) {
        try {
            Scanner scan = new Scanner(new File(file));
            HashMap map = new HashMap();
            while (scan.hasNext()) {
                map.put(scan.nextLine(), "");
            }

            return map;
        } catch (FileNotFoundException ex) {
            if (mysqlenabled) {
                MySqlLogger.logError(ex.toString());
            }
        }
        return null;
    }

    public static Vector getQuery(List list) {
        String query = "";
        Vector map = new Vector();
        Iterator ite = list.iterator();
        String cols = "";
        while (ite.hasNext()) {
            String text = (String) ite.next();
            map.add(text);

            cols = cols + text;
            if (ite.hasNext()) {
                cols = cols + ",";
            }

        }
        if (mysqlenabled) {
            MySqlLogger.logInfo("Columns in dtd," + cols);
        }

        return map;
    }

}
