/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* 
Copyright 2016 iGenie Ltd
*/ 
package com.kislay.mainpack;

import java.util.HashMap;
import org.apache.spark.AccumulatorParam;

/**
 *
 * @author kislay
 */
public class HashMapAccumulator implements AccumulatorParam<HashMap>{

    @Override
    public HashMap addAccumulator(HashMap t, HashMap t1) {
       t.putAll(t1);
       return t;
    }

    @Override
    public HashMap addInPlace(HashMap r, HashMap r1) {
        r.putAll(r1);
        return r;
    }

    @Override
    public HashMap zero(HashMap r) {
        r=new HashMap(0);
        return r;
    }
    
}
