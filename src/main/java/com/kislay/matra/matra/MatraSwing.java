/*
 * Created on Mar 29, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
/* 
Copyright 2016 iGenie Ltd
*/ 
package com.kislay.matra.matra;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import com.kislay.matra.dtdparser.DTDParser;
import com.kislay.matra.exception.DTDException;
import com.kislay.matra.io.DTDFile;
import com.kislay.matra.io.DTDSource;

public class MatraSwing implements ActionListener {

	private JPanel mainPanel, inputPanel, outputPanel;
	private JTextField location = new JTextField(50);
	private JTextField dtdName = new JTextField(20);
	private JTextArea textArea;

	public MatraSwing() {
		inputPanel = new JPanel();
		outputPanel = new JPanel();
		
		//Add various widgets to the sub panels.
		addWidgets();
			  
		//Create the main panel to contain the two sub panels.
		mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.X_AXIS));
		mainPanel.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));

		//Add the select and display panels to the main panel.
		mainPanel.add(inputPanel);
		mainPanel.add(outputPanel);	
	}
	
	private void addWidgets() {
		
		JLabel locationLabel = new JLabel("DTD Location", SwingConstants.LEFT);
		inputPanel.add(locationLabel);
		inputPanel.add(location);
		
		JLabel nameLabel = new JLabel("DTD Name", SwingConstants.LEFT);
		inputPanel.add(nameLabel);
		inputPanel.add(dtdName);

		JButton generateButton = new JButton("Generate");
		//Listen to events from the Generate button.
		generateButton.addActionListener(this);
		inputPanel.add(generateButton);
		
		//output widgets
		textArea = new JTextArea("test");
		textArea.setEditable(false);
		outputPanel.add(textArea);
		
		//Add a border around the select panel.
		inputPanel.setBorder(BorderFactory.createCompoundBorder(
		BorderFactory.createTitledBorder("Input Parameters"), 
		BorderFactory.createEmptyBorder(5,5,5,5)));
		
		//Add a border around the display panel.
		outputPanel.setBorder(BorderFactory.createCompoundBorder(
		BorderFactory.createTitledBorder("Output"), 
		BorderFactory.createEmptyBorder(5,5,5,5)));
	}
	
	public void actionPerformed(ActionEvent event) {
		String dtdLocation = location.getText();
		DTDParser parser = new DTDParser();
		DTDSource dtdSrc = new DTDFile(dtdLocation);
		try {
			parser.parse(dtdSrc);
			textArea.setText(parser.getDocType().toString());
		} catch(DTDException de) {
		}
	}
	
    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    private static void createAndShowGUI() {
        //Make sure we have nice window decorations.
//        JFrame.setDefaultLookAndFeelDecorated(true);

		MatraSwing matra = new MatraSwing();
		
        //Create and set up the window.
        JFrame frame = new JFrame("Matra GUI Interface");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setContentPane(matra.mainPanel);

        //Add the ubiquitous "Hello World" label.
//        JLabel label = new JLabel("Hello World");
//        frame.getContentPane().add(label);

        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }
}

