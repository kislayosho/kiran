/*
 *   The contents of this file are subject to the Mozilla Public License
 *   Version 1.1 (the "License"); you may not use this file except in
 *   compliance with the License. You may obtain a copy of the License at
 *   http://www.mozilla.org/MPL/
 *
 *   Software distributed under the License is distributed on an "AS IS"
 *   basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 *   License for the specific language governing rights and limitations
 *   under the License.
 *
 *   The Original Code is Matra - the DTD Parser.
 *
 *   The Initial Developer of the Original Code is Conrad S Roche.
 *   Portions created by Conrad S Roche are Copyright (C) Conrad 
 *   S Roche. All Rights Reserved.
 *
 *   Alternatively, the contents of this file may be used under the terms
 *   of the GNU GENERAL PUBLIC LICENSE Version 2 or any later version
 *   (the  "[GPL] License"), in which case the
 *   provisions of GPL License are applicable instead of those
 *   above.  If you wish to allow use of your version of this file only
 *   under the terms of the GPL License and not to allow others to use
 *   your version of this file under the MPL, indicate your decision by
 *   deleting  the provisions above and replace  them with the notice and
 *   other provisions required by the GPL License.  If you do not delete
 *   the provisions above, a recipient may use your version of this file
 *   under either the MPL or the GPL License."
 *
 *   [NOTE: The text of this Exhibit A may differ slightly from the text of
 *   the notices in the Source Code files of the Original Code. You should
 *   use the text of this Exhibit A rather than the text found in the
 *   Original Code Source Code for Your Modifications.]
 *
 * Created: Conrad S Roche <derupe at users.sourceforge.net>,  26-Jul-2003
 */
/* 
Copyright 2016 iGenie Ltd
*/ 
package com.kislay.matra.exception;

import com.kislay.matra.data.CursorLocation;

/**
 * Exception denoting a syntax error in the input DTD.
 * 
 * @author Kislay Kumar
 */
public class DTDSyntaxException extends DTDException {

	//CR: TODO: Make this an ArrayList of syntax errors
	private DTDSyntaxErrorBean syntaxError = new DTDSyntaxErrorBean();
	
	//CR: TODO: Add postion and syntax error type information
	//Postion will be row, clm and the DTD Module where found
	//Error type will include if error found which declaration.
	//CR: TODO: Later, also add possible resolution ... like '!' missed out ...
	
	/**
	 * DTDSyntaxException constructor.
	 */
	public DTDSyntaxException() {
		super();
	}

	/**
	 * DTDSyntaxException constructor.
	 * 
	 * @param s Description of the syntax error.
	 */
	public DTDSyntaxException(String s) {
		super(s);
	}

	/**
	 * DTDSyntaxException Constructor.
	 * 
	 * @param s Description of the syntax error.
	 * @param location Location of the syntax error.
	 */
	public DTDSyntaxException(String s, CursorLocation location) {
		super(s);
		syntaxError.setErrorLocation(location);
	}

	/**
	 * Returns the location where the syntax
	 * error was encountered.
	 * 
	 * @return The location of the syntax error.
	 */
	public CursorLocation getErrorLocation() {
		return syntaxError.getErrorLocation();
	}
	
	//CR: TODO: Add methods to add sytax errors
}
