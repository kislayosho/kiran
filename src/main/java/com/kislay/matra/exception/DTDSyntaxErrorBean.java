/*
 *   The contents of this file are subject to the Mozilla Public License
 *   Version 1.1 (the "License"); you may not use this file except in
 *   compliance with the License. You may obtain a copy of the License at
 *   http://www.mozilla.org/MPL/
 *
 *   Software distributed under the License is distributed on an "AS IS"
 *   basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 *   License for the specific language governing rights and limitations
 *   under the License.
 *
 *   The Original Code is Matra - the DTD Parser.
 *
 *   The Initial Developer of the Original Code is Conrad S Roche.
 *   Portions created by Conrad S Roche are Copyright (C) Conrad 
 *   S Roche. All Rights Reserved.
 *
 *   Alternatively, the contents of this file may be used under the terms
 *   of the GNU GENERAL PUBLIC LICENSE Version 2 or any later version
 *   (the  "[GPL] License"), in which case the
 *   provisions of GPL License are applicable instead of those
 *   above.  If you wish to allow use of your version of this file only
 *   under the terms of the GPL License and not to allow others to use
 *   your version of this file under the MPL, indicate your decision by
 *   deleting  the provisions above and replace  them with the notice and
 *   other provisions required by the GPL License.  If you do not delete
 *   the provisions above, a recipient may use your version of this file
 *   under either the MPL or the GPL License."
 *
 *   [NOTE: The text of this Exhibit A may differ slightly from the text of
 *   the notices in the Source Code files of the Original Code. You should
 *   use the text of this Exhibit A rather than the text found in the
 *   Original Code Source Code for Your Modifications.]
 *
 * Created: Conrad S Roche <derupe at users.sourceforge.net>,  30-Mar-2004
 */
/* 
Copyright 2016 iGenie Ltd
*/ 
package com.kislay.matra.exception;

import java.io.Serializable;

import com.kislay.matra.data.CursorLocation;

/**
 * Class to hold information about a
 * sytax error.
 */
public class DTDSyntaxErrorBean implements Serializable {

	/*
	 * Store this information -
	 * location of the error
		location of the start of the construct in which the error occurred
		error code
		error description
		possible fix for the error
	 */

	/**
	 * The location of the cursor where the
	 * syntax error was found.
	 */
	private CursorLocation errorLocation = null;
	
	/**
	 * An error message for the sytax error.
	 */
	private String errorMessage;
	
	public DTDSyntaxErrorBean() {
	}
	
	public DTDSyntaxErrorBean(CursorLocation errorLocation, String errorMessage) {
		this.errorLocation = errorLocation;
		this.errorMessage = errorMessage;
	}

	/**
	 * Sets the error location for this sytax error.
	 * 
	 * @param errorLocation The error location.
	 */
	public void setErrorLocation(CursorLocation errorLocation) {
		this.errorLocation = errorLocation;
	}

	/**
	 * Returns the error location for this sytax error.
	 * 
	 * @return The error location.
	 */
	public CursorLocation getErrorLocation() {
		return errorLocation;
	}

	/**
	 * Sets the error message for this sytax error.
	 * 
	 * @param errorMessage The error message.
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	/**
	 * Returns the error message for this sytax error.
	 * 
	 * @return The error message.
	 */
	public String getErrorMessage() {
		return errorMessage;
	}
}
