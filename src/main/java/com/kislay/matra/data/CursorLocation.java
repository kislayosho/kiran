/*
 *   The contents of this file are subject to the Mozilla Public License
 *   Version 1.1 (the "License"); you may not use this file except in
 *   compliance with the License. You may obtain a copy of the License at
 *   http://www.mozilla.org/MPL/
 *
 *   Software distributed under the License is distributed on an "AS IS"
 *   basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 *   License for the specific language governing rights and limitations
 *   under the License.
 *
 *   The Original Code is Matra - the DTD Parser.
 *
 *   The Initial Developer of the Original Code is Conrad S Roche.
 *   Portions created by Conrad S Roche are Copyright (C) Conrad 
 *   S Roche. All Rights Reserved.
 *
 *   Alternatively, the contents of this file may be used under the terms
 *   of the GNU GENERAL PUBLIC LICENSE Version 2 or any later version
 *   (the  "[GPL] License"), in which case the
 *   provisions of GPL License are applicable instead of those
 *   above.  If you wish to allow use of your version of this file only
 *   under the terms of the GPL License and not to allow others to use
 *   your version of this file under the MPL, indicate your decision by
 *   deleting  the provisions above and replace  them with the notice and
 *   other provisions required by the GPL License.  If you do not delete
 *   the provisions above, a recipient may use your version of this file
 *   under either the MPL or the GPL License."
 *
 *   [NOTE: The text of this Exhibit A may differ slightly from the text of
 *   the notices in the Source Code files of the Original Code. You should
 *   use the text of this Exhibit A rather than the text found in the
 *   Original Code Source Code for Your Modifications.]
 *
 * Created: Conrad S Roche <derupe at users.sourceforge.net>,  28-Sep-2003
 */
/* 
Copyright 2016 iGenie Ltd
*/ 
package com.kislay.matra.data;

/**
 * Class to hold the location of the read cursor 
 * within the data.
 *
 * @author Kislay Kumar
 */
public class CursorLocation {

	/**
	 * The row number at which the
	 * cursor is located.
	 */
	private int row = 0;

	/**
	 * The column number at which the
	 * cursor is located.
	 */
	private int clm = 0;

	/**
	 * The offset, from the data start,
	 * at which the cursor is located.
	 */
	private int offset = 0;
	
	/**
	 * CursorLocation Constructor.
	 */
	private CursorLocation() {
		super();
	}

	/**
	 * CursorLocation Constructor.
	 * 
	 * @param row The row number.
	 * @param clm The column number.
	 * @param offset The offset from the start of the data.
	 */
	public CursorLocation(int row, int clm, int offset) {
		this.row = row;
		this.clm = clm;
		this.offset = offset;
	}
	
	/**
	 * Returns the row number of the cursor.
	 * 
	 * @return The row number.
	 */
	public int getRow() {
		return row;
	}
	
	/**
	 * Returns the column number of the cursor.
	 * 
	 * @return The column number.
	 */
	public int getColumn() {
		return clm;
	}
	
	/**
	 * Returns the offset from the beginning
	 * of the data stream.
	 * 
	 * @return The offset.
	 */
	public int getOffset() {
		return offset;
	}
	
	/**
	 * Returns a string representation
	 * of the cursor location.
	 * 
	 * @return String reporesentation of the location.
	 */
	public String toString() {
		return "(" + row + ", " + clm + ")";
	}
}
