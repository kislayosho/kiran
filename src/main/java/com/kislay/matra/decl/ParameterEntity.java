/*
 *   The contents of this file are subject to the Mozilla Public License
 *   Version 1.1 (the "License"); you may not use this file except in
 *   compliance with the License. You may obtain a copy of the License at
 *   http://www.mozilla.org/MPL/
 *
 *   Software distributed under the License is distributed on an "AS IS"
 *   basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 *   License for the specific language governing rights and limitations
 *   under the License.
 *
 *   The Original Code is Matra - the DTD Parser.
 *
 *   The Initial Developer of the Original Code is Conrad S Roche.
 *   Portions created by Conrad S Roche are Copyright (C) Conrad 
 *   S Roche. All Rights Reserved.
 *
 *   Alternatively, the contents of this file may be used under the terms
 *   of the GNU GENERAL PUBLIC LICENSE Version 2 or any later version
 *   (the  "[GPL] License"), in which case the
 *   provisions of GPL License are applicable instead of those
 *   above.  If you wish to allow use of your version of this file only
 *   under the terms of the GPL License and not to allow others to use
 *   your version of this file under the MPL, indicate your decision by
 *   deleting  the provisions above and replace  them with the notice and
 *   other provisions required by the GPL License.  If you do not delete
 *   the provisions above, a recipient may use your version of this file
 *   under either the MPL or the GPL License."
 *
 *   [NOTE: The text of this Exhibit A may differ slightly from the text of
 *   the notices in the Source Code files of the Original Code. You should
 *   use the text of this Exhibit A rather than the text found in the
 *   Original Code Source Code for Your Modifications.]
 *
 * Created: Conrad S Roche <derupe at users.sourceforge.net>,  10-Sep-2003
 */
/* 
Copyright 2016 iGenie Ltd
*/ 
package com.kislay.matra.decl;

import java.util.Hashtable;

import com.kislay.matra.exception.DTDException;

/**
 * Class to hold a parameter entity declaration.
 *
 * @author Kislay Kumar
 */
public class ParameterEntity extends Entity 
		implements com.kislay.dtd.decl.ParameterEntity {

	/**
	 * ParameterEntity Constructor.
	 */
	public ParameterEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * ParameterEntity Constructor.
	 * 
	 * @param name   The name of the entity.
	 * @param value  The value of the enetity.
	 * @param type   an int specifying the entity type. 
	 */
	public ParameterEntity(String name, String value, int type) {
		super(name, value, type);
		// TODO Auto-generated constructor stub
	}

	/**
	 * ParameterEntity Constructor.
	 * 
	 * @param content The entity declaration.
	 * 
	 * @throws DTDException If any syntax error is encountered.
	 */
	public ParameterEntity(String content) throws DTDException {
		super(content);
		// TODO Auto-generated constructor stub
	}

	/**
	 * ParameterEntity Constructor.
	 * 
	 * @param content The entity declaration.
	 * @param EntityList  a Hashtable object
	 * 
	 * @throws DTDException If any syntax error is encountered.
	 */
	public ParameterEntity(String content, Hashtable EntityList)
		throws DTDException {
		super(content, EntityList);
		// TODO Auto-generated constructor stub
	}

}
