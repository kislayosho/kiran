/*
 *   The contents of this file are subject to the Mozilla Public License
 *   Version 1.1 (the "License"); you may not use this file except in
 *   compliance with the License. You may obtain a copy of the License at
 *   http://www.mozilla.org/MPL/
 *
 *   Software distributed under the License is distributed on an "AS IS"
 *   basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 *   License for the specific language governing rights and limitations
 *   under the License.
 *
 *   The Original Code is Matra - the DTD Parser.
 *
 *   The Initial Developer of the Original Code is Conrad S Roche.
 *   Portions created by Conrad S Roche are Copyright (C) Conrad 
 *   S Roche. All Rights Reserved.
 *
 *   Alternatively, the contents of this file may be used under the terms
 *   of the GNU GENERAL PUBLIC LICENSE Version 2 or any later version
 *   (the  "[GPL] License"), in which case the
 *   provisions of GPL License are applicable instead of those
 *   above.  If you wish to allow use of your version of this file only
 *   under the terms of the GPL License and not to allow others to use
 *   your version of this file under the MPL, indicate your decision by
 *   deleting  the provisions above and replace  them with the notice and
 *   other provisions required by the GPL License.  If you do not delete
 *   the provisions above, a recipient may use your version of this file
 *   under either the MPL or the GPL License."
 *
 *   [NOTE: The text of this Exhibit A may differ slightly from the text of
 *   the notices in the Source Code files of the Original Code. You should
 *   use the text of this Exhibit A rather than the text found in the
 *   Original Code Source Code for Your Modifications.]
 *
 * Created: Conrad S Roche <derupe at users.sourceforge.net>,  19-Oct-2003
 */
/* 
Copyright 2016 iGenie Ltd
*/ 
package com.kislay.matra.decl;

/**
 * Class to hold the details of a processing instruction 
 * in the DTD.  
 *
 * @author Kislay Kumar
 */
public class PI implements com.kislay.dtd.decl.PI {

	/**
	 * The PI Target.
	 */
	private String target;
	
	/**
	 * The PI instructions.
	 */
	private String instructions;
	
	/**
	 * PI Constructor.
	 */
	public PI() {
		super();
	}

	/**
	 * Returns the PI Target.
	 * 
	 * @see com.conradroche.dtd.decl.PI#getPITarget()
	 * @see #setPITarget
	 * 
	 * @return The PI Target.
	 */
	public String getPITarget() {
		return target;
	}

	/**
	 * Sets the Target for this PI.
	 * 
	 * @see com.conradroche.dtd.decl.PI#setPITarget(java.lang.String)
	 * @see #getPITarget
	 * 
	 * @param target The target for this PI.
	 */
	public void setPITarget(String target) {
		this.target = target;
	}

	/**
	 * Returns the character data that constitute
	 * the instructions.
	 * 
	 * @see com.conradroche.dtd.decl.PI#getInstructions()
	 * @see #setInstructions
	 * 
	 * @return The instructions.
	 */
	public String getInstructions() {
		return instructions;
	}

	/**
	 * Sets the character data that constitute
	 * the instructions.
	 * 
	 * @see com.conradroche.dtd.decl.PI#setInstructions(java.lang.String)
	 * @see #getInstructions
	 * 
	 * @param data The instructions.
	 */
	public void setInstructions(String data) {
		instructions = data;
	}
}