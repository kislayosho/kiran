/*
 *   The contents of this file are subject to the Mozilla Public License
 *   Version 1.1 (the "License"); you may not use this file except in
 *   compliance with the License. You may obtain a copy of the License at
 *   http://www.mozilla.org/MPL/
 *
 *   Software distributed under the License is distributed on an "AS IS"
 *   basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 *   License for the specific language governing rights and limitations
 *   under the License.
 *
 *   The Original Code is Matra - the DTD Parser.
 *
 *   The Initial Developer of the Original Code is Conrad S Roche.
 *   Portions created by Conrad S Roche are Copyright (C) Conrad 
 *   S Roche. All Rights Reserved.
 *
 *   Alternatively, the contents of this file may be used under the terms
 *   of the GNU GENERAL PUBLIC LICENSE Version 2 or any later version
 *   (the  "[GPL] License"), in which case the
 *   provisions of GPL License are applicable instead of those
 *   above.  If you wish to allow use of your version of this file only
 *   under the terms of the GPL License and not to allow others to use
 *   your version of this file under the MPL, indicate your decision by
 *   deleting  the provisions above and replace  them with the notice and
 *   other provisions required by the GPL License.  If you do not delete
 *   the provisions above, a recipient may use your version of this file
 *   under either the MPL or the GPL License."
 *
 *   [NOTE: The text of this Exhibit A may differ slightly from the text of
 *   the notices in the Source Code files of the Original Code. You should
 *   use the text of this Exhibit A rather than the text found in the
 *   Original Code Source Code for Your Modifications.]
 *
 * Created: Conrad S Roche <derupe at users.sourceforge.net>,  21-Sep-2003
 */
/* 
Copyright 2016 iGenie Ltd
*/ 
package com.kislay.matra.decl;

/**
 * Class to hold the external identified for the 
 * attribute or notation declaration.
 *
 * @author Kislay Kumar
 */
class ExternalID implements com.kislay.dtd.decl.ExternalID {

	private String sysID = null;
	private String pubID = null;

	/**
	 * ExternalID Constructor.
	 */	
	public ExternalID() {
		super();
	}

	/**
	 * Returns the System Identifier.
	 * 
	 * @return The System Identifier
	 * 
	 * @see #setSystemIdentifier
	 */
	public String getSystemIdentifier() {
		return sysID;
	}

	/**
	 * Sets the System Identifier.
	 * 
	 * @see #getSystemIdentifier
	 * 
	 * @param systemId The System Identifier
	 */
	public void setSystemIdentifier(String systemId) {
		sysID = systemId;
	}
	
	/**
	 * Returns the Public Identifier.
	 * 
	 * @see #setPublicIdentifier
	 * 
	 * @return The Public Identifier
	 */
	public String getPublicIdentifier() {
		return pubID;
	}

	/**
	 * Sets the Public Identifier.
	 * 
	 * @see #getPublicIdentifier
	 * 
	 * @param publicId The Public Identifier
	 */
	public void setPublicIdentifier(String publicId) {
		pubID = publicId;
	}
	
	/**
	 * Returns <code>true</code> if the 
	 * System Identifier is specified.
	 * 
	 * @return <code>true</code> if the 
	 * 		System Identifier is specified
	 * 		<code>false</code> otherwise.
	 */	
	public boolean hasSystemIdentifier() {
		
		if(sysID == null) {
			return false;
		}
		
		return true;
	}

	/**
	 * Returns <code>true</code> if the 
	 * Public Identifier is specified.
	 * 
	 * @return <code>true</code> if the 
	 * 		Public Identifier is specified
	 * 		<code>false</code> otherwise.
	 */	
	public boolean hasPublicIdentifier() {
		
		if(pubID == null) {
			return false;
		}
		
		return true;
	}
}
