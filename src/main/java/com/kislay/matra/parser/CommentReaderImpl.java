/*
 *   The contents of this file are subject to the Mozilla Public License
 *   Version 1.1 (the "License"); you may not use this file except in
 *   compliance with the License. You may obtain a copy of the License at
 *   http://www.mozilla.org/MPL/
 *
 *   Software distributed under the License is distributed on an "AS IS"
 *   basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 *   License for the specific language governing rights and limitations
 *   under the License.
 *
 *   The Original Code is Matra - the DTD Parser.
 *
 *   The Initial Developer of the Original Code is Conrad S Roche.
 *   Portions created by Conrad S Roche are Copyright (C) Conrad 
 *   S Roche. All Rights Reserved.
 *
 *   Alternatively, the contents of this file may be used under the terms
 *   of the GNU GENERAL PUBLIC LICENSE Version 2 or any later version
 *   (the  "[GPL] License"), in which case the
 *   provisions of GPL License are applicable instead of those
 *   above.  If you wish to allow use of your version of this file only
 *   under the terms of the GPL License and not to allow others to use
 *   your version of this file under the MPL, indicate your decision by
 *   deleting  the provisions above and replace  them with the notice and
 *   other provisions required by the GPL License.  If you do not delete
 *   the provisions above, a recipient may use your version of this file
 *   under either the MPL or the GPL License."
 *
 *   [NOTE: The text of this Exhibit A may differ slightly from the text of
 *   the notices in the Source Code files of the Original Code. You should
 *   use the text of this Exhibit A rather than the text found in the
 *   Original Code Source Code for Your Modifications.]
 *
 * Created: Conrad S Roche <derupe at users.sourceforge.net>,  17-Sep-2003
 */
/* 
Copyright 2016 iGenie Ltd
*/ 
package com.kislay.matra.parser;

import com.kislay.dtd.decl.Comment;
import com.kislay.dtd.parser.CommentReader;
import com.kislay.matra.data.DTDData;
import com.kislay.matra.exception.DTDSyntaxException;

/**
 * Class to read the comment text.
 *
 * @author Kislay Kumar
 */
public class CommentReaderImpl implements CommentReader {

	/**
	 * Token identifying a comment.
	 */
	private static final String COMMENT_START = "<!--";
	
	/**
	 * CommentReaderImpl Constructor.
	 */
	public CommentReaderImpl() {
		super();
	}

	/**
	 * Read the comment from the data stream.
	 * 
	 * @param data The stream from which to read the comment.
	 * 
	 * @return The comment.
	 * 
	 * @throws DTDSyntaxException If the comment 
	 * 			contains a syntax error. 
	 */
	public Comment readComment(DTDData data) throws DTDSyntaxException {
		
		if(!isCommentStart(data)) {
			return null; 
		}
	
		StringBuffer comment = new StringBuffer("");
	
		//read the '<!--' comment beginning
		data.skipChars(COMMENT_START.length());
		
		while( !data.endOfData() ) {
			
			if(data.lookNextChar() == '-' && data.peekAhead(2) == '-') { //end of comment
				
				data.skipChars(2); //'--'
				if(data.lookNextChar() != '>') {
					throw new DTDSyntaxException("Invalid comment block encountered at location " + 
									data.getCurrentLocation() + ".");
				}
				
				data.getNextChar(); // '>'
				
				Comment cmnt = new com.kislay.matra.decl.Comment();
				cmnt.setCommentText(comment.toString());
				return cmnt;
			}
			
			comment.append(data.getNextChar());
		}
		
		//Got End of Data before reaching '-->'
		throw new DTDSyntaxException("Invalid comment block encountered at location " + data.getCurrentLocation() + ".");
	}
	
	/**
	 * Checks if there is a comment at the 
	 * beginning of the data.
	 * 
	 * @param data The data to be parsed.
	 * 
	 * @return <code>true</code> if the current location
	 * 	of the data has a comment; <code>false</code> otherwise.
	 */
	public boolean isCommentStart(DTDData data) {
		
		if(data == null || data.endOfData()) {
			return false;
		}
		
		return data.nextStringEquals(COMMENT_START);
	}
}
