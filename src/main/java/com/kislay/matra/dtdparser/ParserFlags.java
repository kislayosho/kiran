/*
 *   The contents of this file are subject to the Mozilla Public License
 *   Version 1.1 (the "License"); you may not use this file except in
 *   compliance with the License. You may obtain a copy of the License at
 *   http://www.mozilla.org/MPL/
 *
 *   Software distributed under the License is distributed on an "AS IS"
 *   basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 *   License for the specific language governing rights and limitations
 *   under the License.
 *
 *   The Original Code is Matra - the DTD Parser.
 *
 *   The Initial Developer of the Original Code is Conrad S Roche.
 *   Portions created by Conrad S Roche are Copyright (C) Conrad 
 *   S Roche. All Rights Reserved.
 *
 *   Alternatively, the contents of this file may be used under the terms
 *   of the GNU GENERAL PUBLIC LICENSE Version 2 or any later version
 *   (the  "[GPL] License"), in which case the
 *   provisions of GPL License are applicable instead of those
 *   above.  If you wish to allow use of your version of this file only
 *   under the terms of the GPL License and not to allow others to use
 *   your version of this file under the MPL, indicate your decision by
 *   deleting  the provisions above and replace  them with the notice and
 *   other provisions required by the GPL License.  If you do not delete
 *   the provisions above, a recipient may use your version of this file
 *   under either the MPL or the GPL License."
 *
 *   [NOTE: The text of this Exhibit A may differ slightly from the text of
 *   the notices in the Source Code files of the Original Code. You should
 *   use the text of this Exhibit A rather than the text found in the
 *   Original Code Source Code for Your Modifications.]
 *
 * Created: Conrad S Roche <derupe at users.sourceforge.net>,  03-Aug-2003
 */
/* 
Copyright 2016 iGenie Ltd
*/ 
package com.kislay.matra.dtdparser;

/**
 * Class to hold the various flag that would determine
 * the warnings displayed to the user.
 * 
 * @author Kislay Kumar
 */
public class ParserFlags {


	//VC_XXX - Validity Constraint
	//WFC_XXX - Well Formedness Constraint
	//UO_XXX - User Option
	
	/**
	 * 3.2 Element Type Declarations [XML Rec 1.0]<p/>
	 * 
	 * At user option, an XML processor may issue a warning when a 
	 * declaration mentions an element type for which no declaration 
	 * is provided, but this is not an error.
	 */
	public static boolean UO_MISSING_ELEM_DECL = true;
	 
	/**
	 * 3.3 Attribute-List Declarations [XML Rec 1.0]<p/>
	 * 
	 * At user option, an XML processor may issue a warning if attributes 
	 * are declared for an element type not itself declared, but this is 
	 * not an error.
	 */
	public static boolean UO_ATTR_UNKNOWN_ELEM = true;
	
	/**
	 * 3.3 Attribute-List Declarations [XML Rec 1.0]<p/>
	 * 
	 * For interoperability, an XML processor may at user option issue a 
	 * warning when more than one attribute-list declaration is provided 
	 * for a given element type, or more than one attribute definition is 
	 * provided for a given attribute, but this is not an error.
	 */
	public static boolean UO_MULTIPLE_ATTR_LIST = true;
	
	/**
	 * Constructs a ParserFlags object.
	 */
	public ParserFlags() {
		super();
	}
}
