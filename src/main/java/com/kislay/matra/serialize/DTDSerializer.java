/*
 *   The contents of this file are subject to the Mozilla Public License
 *   Version 1.1 (the "License"); you may not use this file except in
 *   compliance with the License. You may obtain a copy of the License at
 *   http://www.mozilla.org/MPL/
 *
 *   Software distributed under the License is distributed on an "AS IS"
 *   basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 *   License for the specific language governing rights and limitations
 *   under the License.
 *
 *   The Original Code is Matra - the DTD Parser.
 *
 *   The Initial Developer of the Original Code is Conrad S Roche.
 *   Portions created by Conrad S Roche are Copyright (C) Conrad 
 *   S Roche. All Rights Reserved.
 *
 *   Alternatively, the contents of this file may be used under the terms
 *   of the GNU GENERAL PUBLIC LICENSE Version 2 or any later version
 *   (the  "[GPL] License"), in which case the
 *   provisions of GPL License are applicable instead of those
 *   above.  If you wish to allow use of your version of this file only
 *   under the terms of the GPL License and not to allow others to use
 *   your version of this file under the MPL, indicate your decision by
 *   deleting  the provisions above and replace  them with the notice and
 *   other provisions required by the GPL License.  If you do not delete
 *   the provisions above, a recipient may use your version of this file
 *   under either the MPL or the GPL License."
 *
 *   [NOTE: The text of this Exhibit A may differ slightly from the text of
 *   the notices in the Source Code files of the Original Code. You should
 *   use the text of this Exhibit A rather than the text found in the
 *   Original Code Source Code for Your Modifications.]
 *
 * Created: Conrad S Roche <derupe at users.sourceforge.net>,  30-Jul-2003
 */
/* 
Copyright 2016 iGenie Ltd
*/ 
package com.kislay.matra.serialize;

import java.util.Collection;

import com.kislay.matra.decl.AttList;
import com.kislay.matra.decl.ElementType;

/**
 * Class to serialize the in-memory dtd.
 * 
 * @author Kislay Kumar
 */
public class DTDSerializer {

	private Collection elementList;
	private AttList attributeList;
	
	/**
	 * Holds the serialized output.
	 */
	private StringBuffer dtdStr = new StringBuffer("");
	
	/**
	 * Constructs a DTDSerializer object.
	 * 
	 * @param rootElement The name of the root element.
	 * @param elemList Collection of <code>ElementType</code>.
	 * @param attList Collection of <code>AttList</code>.
	 */
	public DTDSerializer(String rootElement, Collection elemList, AttList attList) {
	}
	
	private void appendElementDetails(String elemName) {
		
//		ElementType element = elementList.
	}
	
	private void appendElement(ElementType element) {
		
		dtdStr.append("<!ELEMENT ").append(element.getName());

		//use shortcut!!!
//		str += getContentSpec();
//		str += ">";
	}
	
	private void appendAttList(String elemName) {
	}
}
