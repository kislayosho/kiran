/*
 *   The contents of this file are subject to the Mozilla Public License
 *   Version 1.1 (the "License"); you may not use this file except in
 *   compliance with the License. You may obtain a copy of the License at
 *   http://www.mozilla.org/MPL/
 *
 *   Software distributed under the License is distributed on an "AS IS"
 *   basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 *   License for the specific language governing rights and limitations
 *   under the License.
 *
 *   The Original Code is Matra - the DTD Parser.
 *
 *   The Initial Developer of the Original Code is Conrad S Roche.
 *   Portions created by Conrad S Roche are Copyright (C) Conrad 
 *   S Roche. All Rights Reserved.
 *
 *   Alternatively, the contents of this file may be used under the terms
 *   of the GNU GENERAL PUBLIC LICENSE Version 2 or any later version
 *   (the  "[GPL] License"), in which case the
 *   provisions of GPL License are applicable instead of those
 *   above.  If you wish to allow use of your version of this file only
 *   under the terms of the GPL License and not to allow others to use
 *   your version of this file under the MPL, indicate your decision by
 *   deleting  the provisions above and replace  them with the notice and
 *   other provisions required by the GPL License.  If you do not delete
 *   the provisions above, a recipient may use your version of this file
 *   under either the MPL or the GPL License."
 *
 *   [NOTE: The text of this Exhibit A may differ slightly from the text of
 *   the notices in the Source Code files of the Original Code. You should
 *   use the text of this Exhibit A rather than the text found in the
 *   Original Code Source Code for Your Modifications.]
 *
 * Created: Conrad S Roche <derupe at users.sourceforge.net>,  16-Aug-2003
 */
/* 
Copyright 2016 iGenie Ltd
*/ 
package com.kislay.matra.example;

import com.kislay.matra.dtdparser.DTDParser;
import com.kislay.matra.exception.DTDException;
import com.kislay.matra.io.DTDUrl;
import com.kislay.matra.tree.DTDTree;

/**
 * Sample code using Matra to 
 * parse a DTD file whose location 
 * is specified as a URL.
 * 
 * @author Kislay Kumar
 */
public class ParseDTDUrl {

	/**
	 * ParseDTDUrl constructor.
	 */
	public ParseDTDUrl() {
		super();
	}

	/**
	 * Main Method.
	 * 
	 * @param args Command Line arguments. 
	 *         Will contain the URL of the 
	 *         DTD to be parsed.
	 *  
	 */
	public static void main(String[] args) {
		
		String url;
		
		//check if the filename was provided at the command line.
		if(args.length == 0) {
			//NOTE: you can specify the DTD URL location here.
			url = "http://dtd/location/name.dtd";
			System.out.println("Command line argument not passed ... assuming location " + url);
			System.exit(1);
		} else {
			url = args[0];
		}
		
		DTDUrl dtdUrl = new DTDUrl(url);
		DTDParser parser = new DTDParser();
		/*
		 * DTDParser: 
		 * 
		 * This is the main parsing class. Calling apps will be 
		 * invoking this class directly to do the DTD Parsing; they
		 * should not call any of the other parsing classes directly.
		 * 
		 * parse(DTDSource)
		 *    If you have the DTD on the internet, then pass a 
		 *    DTDUrl object containing the URL of the DTD file. 
		 * 
		 * The above parse method will, obviously, parse the DTD. If
		 * it encounters any problem - say, if the url could not be
		 * reached or if it has a syntax error - it will throw a DTDException.
		 * 
		 * There is one more method of interest in the DTDParser
		 * 
		 * DocType getDocType()
		 *    This method will return the "DocType" - the parsed DTD 
		 *    object. Call this method only after a successful parse;
		 *    if not, you'll get a DTDException.
		 */
		 
		/*
		 * DTDUrl:
		 * 
		 *    Use this if the DTD is located on the internet. Make sure
		 *    that, either you are the owner of that URL or that the
		 *    owner doesn't have an issue with you connecting to the
		 *    site and downloading the DTD using automated means.
		 *  
		 *    The DTDUrl object will help the DTDParser connect to
		 *    and read from the URL - taking that burden off the application.
		 *    Use the constructor DTDUrl(url)
		 * 
		 *    Where url is the complete URL for the DTD location.
		 */
		 
		/*
		 * DTDTree:
		 * 
		 * The DTDTree is the tree generator for the DTD. It will display
		 * the structure of the DTD in a simplified tree format. Its a great 
		 * way to learn the structure of a mew DTD. NOTE: It *simplifies* the
		 * structure of the DTD for a quick visual overview, the actual
		 * structure that the DTD describes could be more complex. It just 
		 * looks at the parent-child relationships between the Element Types
		 * and not the complex content specs. After getting a high level 
		 * overview using the generated tree, go through the dtd to understand 
		 * it more fully.  
		 */
		
		try {
			System.out.println("=========== Parsing DTD  " + url + " ===============");
			parser.parse(dtdUrl);
					
			//Print the Tree
			DTDTree tree = new DTDTree(parser);
			tree.printTrees();
					
			//Print the merged DTD
			System.out.println("\n=========== Merged DTD ===============");
			System.out.println(parser.getDocType().toString()); //this will be deprecated! 

		} catch(DTDException dtdEx) {
			System.out.println("Got DTDException while parsing the file - ");
			dtdEx.printStackTrace();
		}
	}
}
