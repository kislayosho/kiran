/*
 *   The contents of this file are subject to the Mozilla Public License
 *   Version 1.1 (the "License"); you may not use this file except in
 *   compliance with the License. You may obtain a copy of the License at
 *   http://www.mozilla.org/MPL/
 *
 *   Software distributed under the License is distributed on an "AS IS"
 *   basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 *   License for the specific language governing rights and limitations
 *   under the License.
 *
 *   The Original Code is Matra - the DTD Parser.
 *
 *   The Initial Developer of the Original Code is Conrad S Roche.
 *   Portions created by Conrad S Roche are Copyright (C) Conrad 
 *   S Roche. All Rights Reserved.
 *
 *   Alternatively, the contents of this file may be used under the terms
 *   of the GNU GENERAL PUBLIC LICENSE Version 2 or any later version
 *   (the  "[GPL] License"), in which case the
 *   provisions of GPL License are applicable instead of those
 *   above.  If you wish to allow use of your version of this file only
 *   under the terms of the GPL License and not to allow others to use
 *   your version of this file under the MPL, indicate your decision by
 *   deleting  the provisions above and replace  them with the notice and
 *   other provisions required by the GPL License.  If you do not delete
 *   the provisions above, a recipient may use your version of this file
 *   under either the MPL or the GPL License."
 *
 *   [NOTE: The text of this Exhibit A may differ slightly from the text of
 *   the notices in the Source Code files of the Original Code. You should
 *   use the text of this Exhibit A rather than the text found in the
 *   Original Code Source Code for Your Modifications.]
 *
 * Created: Conrad S Roche <derupe at users.sourceforge.net>,  25-Jul-2000
 */

/* 
Copyright 2016 iGenie Ltd
*/ 
package com.kislay.matra.io;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import com.kislay.matra.data.DTDData;
import com.kislay.matra.exception.DTDException;

/**
 * Class to do the io operations on a File.
 *
 * @author Kislay Kumar
 */
public class DTDFile extends DTDSource {

	/**
	 * The folder in which the dtd file is located.
	 */
	private String dir;
	
	/**
	 * The name of the dtd file.
	 */
	private String filename;
	
	/**
	 * The path separator.
	 * This will be '/' for unix files
	 * and '\' for windows files.
	 */
	private String separator = "";
	
	/**
	 * DTDFile constructor.
	 * 
	 * @param filename The location of the DTD file.
	 */
	public DTDFile(String filename) {
		super(filename);
		
		if(filename == null || filename.equals(""))
			throw new IllegalArgumentException();
	
		int dirSepLocn = filename.lastIndexOf('\\');
		separator = "\\";
		
		if(dirSepLocn == -1) {
			dirSepLocn = filename.lastIndexOf('/');
			if(dirSepLocn == -1) {
				dirSepLocn = 0;
			} else {
				separator = "/";
			}
		} else {
			separator = "\\";
		}
		
		dir = filename.substring(0, dirSepLocn);
		this.filename = filename.substring(dirSepLocn + (dirSepLocn>0 ? 1 : 0), filename.length());
	}

	/**
	 * Return the absolute path of the source
	 * using the path relative to this source.
	 *
	 * @return The abosolute path of the source.
	 * @param relative The relative path of the source w.r.t. 
	 * 			the current location of this source.
	 */
	public DTDSource getAbsolute(String relative) {
	
		//CR: FIXME: take care of unix filesystem also - use FILE class?
		
		String newFile;
		
		if( relative.indexOf(':') != -1 || relative.charAt(0) == '\\' 
			|| relative.charAt(0) == '/' ) { //if the new filename is an absolute path
			newFile = relative;
		} else {
//			newFile = dir + '\\' + relative; //assumes '\\' as the separator.
			newFile = dir + separator + relative; //assumes '\\' as the separator.
		}
	
		return new DTDFile(newFile);
	}

	/**
	 * Returns the name of the DTD File.
	 * 
	 * @return The name of the DTD File.
	 */
	public String getDTDFilename() {
		return filename;
	}

	/**
	 * Returns the directory in which the 
	 * DTD File is placed.
	 * 
	 * @return The directory in which the DTD 
	 * 			File is placed.
	 */
	public String getDTDDirectory() {
		return filename;
	}

	/**
	 * Method to perform the read operation on this file source.
	 * 
	 * @return The DTDData object containing all the read data
	 * 			from the current source.
	 * 
	 * @throws DTDException If the file could not be read.
	 */
	public DTDData read() throws DTDException {
	
		if(filename == null || filename.equals(""))
			throw new IllegalArgumentException();
	
		String location = "";
		
		if(dir != null) {
			//CR: TODO: Keep track of the separator!
//			location = dir + "\\" + filename; 
			location = dir + separator + filename; 
		}
		try {
			BufferedReader in = new BufferedReader( new InputStreamReader( new BufferedInputStream( 
									new FileInputStream(location) ) ) );
			
			String line = null;
			String dtdStr = "";
			
			while((line = in.readLine())!= null)
				dtdStr += line + "\n";
				
			in.close();
			
			//debug
	//		System.out.println(dtdStr);
			
			return new DTDData(dtdStr);
		}
		catch (FileNotFoundException e) {
//			e.printStackTrace();
			//CR: TODO: nest this exception
			throw new DTDException("Specified DTD file (" + location + ") not found.");
		}
		catch (IOException e) {
//			e.printStackTrace();
			//CR: TODO: nest this exception
			throw new DTDException("IO Exception reading file " + location + ".");
		}
	}
}

