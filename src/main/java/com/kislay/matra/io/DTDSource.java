/*
 *   The contents of this file are subject to the Mozilla Public License
 *   Version 1.1 (the "License"); you may not use this file except in
 *   compliance with the License. You may obtain a copy of the License at
 *   http://www.mozilla.org/MPL/
 *
 *   Software distributed under the License is distributed on an "AS IS"
 *   basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 *   License for the specific language governing rights and limitations
 *   under the License.
 *
 *   The Original Code is Matra - the DTD Parser.
 *
 *   The Initial Developer of the Original Code is Conrad S Roche.
 *   Portions created by Conrad S Roche are Copyright (C) Conrad 
 *   S Roche. All Rights Reserved.
 *
 *   Alternatively, the contents of this file may be used under the terms
 *   of the GNU GENERAL PUBLIC LICENSE Version 2 or any later version
 *   (the  "[GPL] License"), in which case the
 *   provisions of GPL License are applicable instead of those
 *   above.  If you wish to allow use of your version of this file only
 *   under the terms of the GPL License and not to allow others to use
 *   your version of this file under the MPL, indicate your decision by
 *   deleting  the provisions above and replace  them with the notice and
 *   other provisions required by the GPL License.  If you do not delete
 *   the provisions above, a recipient may use your version of this file
 *   under either the MPL or the GPL License."
 *
 *   [NOTE: The text of this Exhibit A may differ slightly from the text of
 *   the notices in the Source Code files of the Original Code. You should
 *   use the text of this Exhibit A rather than the text found in the
 *   Original Code Source Code for Your Modifications.]
 *
 * Created: Conrad S Roche <derupe at users.sourceforge.net>,  25-Jul-2000
 */

/* 
Copyright 2016 iGenie Ltd
*/ 
package com.kislay.matra.io;

import com.kislay.matra.data.DTDData;
import com.kislay.matra.exception.DTDException;

/**
 * Base class for the various classes
 * performing io operations.
 * 
 * @author: Conrad Roche
 */
public abstract class DTDSource {

	/**
	 * The location of the DTD source.
	 */
	protected String location;
	
	/**
	 * DTDSource Constructor.
	 * 
	 * @param dtdLocation The location of the DTD.
	 */
	public DTDSource(String dtdLocation) {
		
		location = dtdLocation;
	}
	
	/**
	 * DTDSource constructor.
	 * 
	 * @param base The base location.
	 * @param relative The relative location.
	 */
	public DTDSource(String base, String relative) {
	
		//CR: NOTE: Should I leave this for the children to handle?
		throw new Error("Not Implemented."); 
	}
	
	/**
	 * Return the absolute path of the source
	 * using the path relative to this source.
	 *
	 * @return The abosolute path of the source.
	 * @param relative The relative path of the source w.r.t. 
	 * 			the current location of this source.
	 */
	public abstract DTDSource getAbsolute(String relative);
	
	/**
	 * Returns the name of the DTD File.
	 * 
	 * @return The name of the DTD File.
	 */
	public abstract String getDTDFilename();
	
	/**
	 * The location of the DTD to which this
	 * is pointing to.
	 * 
	 * @return The location of the DTD.
	 */
	public String getDTDLocation() {
	
		return location;
	}
	
	/**
	 * Method to perform the read operation on this source.
	 * 
	 * @return The DTDData object containing all the read data
	 * 			from the current source.
	 * 
	 * @throws DTDException When the dtd source could not be read. 
	 */
	public abstract DTDData read() throws DTDException;
}

