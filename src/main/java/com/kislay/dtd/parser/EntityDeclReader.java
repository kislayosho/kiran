/*
 */
/* 
Copyright 2016 iGenie Ltd
*/ 
package com.kislay.dtd.parser;

import com.kislay.dtd.decl.Entity;
import com.kislay.matra.data.DTDData;
import com.kislay.matra.exception.DTDSyntaxException;

/**
 * Interface for reading entity declarations.
 *
 * @author Kislay Kumar
 */
public interface EntityDeclReader {
	
	/**
	 * Read the entity declarations from 
	 * the data stream.
	 * 
	 * @param data The stream from which to 
	 * 			read the entity declaration.
	 * 
	 * @return The entity declaration.
	 * 
	 * @throws DTDSyntaxException If the Entity 
	 * 			declaration contains a syntax
	 * 			error. 
	 */
	public Entity readEntityDecl(DTDData data) throws DTDSyntaxException;
	
	/**
	 * Checks if there is an entity declaration
	 * at the current location of the data.
	 * 
	 * @param data The data to be parsed.
	 * 
	 * @return <code>true</code> if the current location
	 * 	of the data has an entity declaration; 
	 * 	<code>false</code> otherwise.
	 */
	public boolean isEntityDeclStart(DTDData data);
}
