/*
 */
/* 
Copyright 2016 iGenie Ltd
*/ 
package com.kislay.dtd.parser;

import com.kislay.dtd.decl.PI;
import com.kislay.matra.data.DTDData;
import com.kislay.matra.exception.DTDSyntaxException;

/**
 * Interface for reading processing
 * instructions.
 *
 * @author Kislay Kumar
 */
public interface PIReader {

	/**
	 * Read the processing instruction from the data stream.
	 * 
	 * @param data The stream from which to read the processing 
	 * 			instruction.
	 * 
	 * @return The processing instruction.
	 * 
	 * @throws DTDSyntaxException If the processing instruction 
	 * 			contains a syntax error. 
	 */
	public PI readPI(DTDData data) throws DTDSyntaxException;
	
	/**
	 * Checks if there is a processing instruction at the 
	 * current location of the data.
	 * 
	 * @param data The data to be parsed.
	 * 
	 * @return <code>true</code> if the current location
	 * 		of the data has a processing instruction; 
	 * 		<code>false</code> otherwise.
	 */
	public boolean isPIStart(DTDData data);
}

