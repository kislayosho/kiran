/*
 */
/* 
Copyright 2016 iGenie Ltd
*/ 
package com.kislay.dtd.parser;

import com.kislay.dtd.decl.Notation;
import com.kislay.matra.data.DTDData;
import com.kislay.matra.exception.DTDSyntaxException;

/**
 * Interface for reading Notation declarations.
 *
 * @author Kislay Kumar
 */
public interface NotationReader {

	/**
	 * Read the notation declaration from the data stream.
	 * 
	 * @param data The stream from which to read the comment.
	 * 
	 * @return The comment.
	 * 
	 * @throws DTDSyntaxException If the Notation 
	 * 			declaration contains a syntax error. 
	 */
	public Notation readNotation(DTDData data) throws DTDSyntaxException;
	
	/**
	 * Checks if there is a notation declaration at the 
	 * current location of the data.
	 * 
	 * @param data The data to be parsed.
	 * 
	 * @return <code>true</code> if the current location
	 * 	of the data has a comment; <code>false</code> otherwise.
	 */
	public boolean isNotationStart(DTDData data);
}
