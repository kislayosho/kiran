/*
 */
/* 
Copyright 2016 iGenie Ltd
*/ 
package com.kislay.dtd.parser;

import com.kislay.dtd.decl.ElementDecl;
import com.kislay.matra.data.DTDData;
import com.kislay.matra.exception.DTDSyntaxException;

/**
 * Interface for reading element declaration.
 *
 * @author Kislay Kumar
 */
public interface ElementDeclReader {

	/**
	 * Read the Element declaration from the
	 * data stream.
	 * 
	 * @param data The stream from which to read the
	 * 			element declaration.
	 * 
	 * @return The element declaration.
	 * 
	 * @throws DTDSyntaxException If the Element 
	 * 			declaration contains a syntax
	 * 			error. 
	 */
	public ElementDecl readElementDecl(DTDData data) throws DTDSyntaxException;
	
	/**
	 * Checks if there is an element declaration
	 * at the current location of the data.
	 * 
	 * @param data The stream from which to read the
	 * 			element declaration.
	 * 
	 * @return <code>true</code> if there is an 
	 * 		element declaration at the start of
	 * 		the data stream; <code>false</code>
	 * 		otherwise.
	 */
	public boolean isElementDeclStart(DTDData data);
}
