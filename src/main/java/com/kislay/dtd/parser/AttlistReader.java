/*
 */
/* 
Copyright 2016 iGenie Ltd
*/ 
package com.kislay.dtd.parser;

import com.kislay.dtd.decl.AttlistDecl;
import com.kislay.matra.data.DTDData;
import com.kislay.matra.exception.DTDSyntaxException;

/**
 * Interface for reading an attribute
 * list declaration.
 *
 * @author Kislay Kumar
 */
public interface AttlistReader {

	/**
	 * Read the Attribute list declaration 
	 * from the data stream.
	 * 
	 * @param data The stream from which to read the
	 * 			Attribute list declaration.
	 * 
	 * @return The Attribute list.
	 * 
	 * @throws DTDSyntaxException If the attribute 
	 * 			list declaration contains a syntax
	 * 			error. 
	 */
	public AttlistDecl readAttlist(DTDData data) throws DTDSyntaxException;
	
	/**
	 * Checks if there is an attribute list
	 * declaration at the current location 
	 * of the data.
	 * 
	 * @param data The data to be parsed.
	 * 
	 * @return <code>true</code> if the current location
	 * 	of the data has an attrubute list declaration; 
	 * 	<code>false</code> otherwise.
	 */
	public boolean isAttlistStart(DTDData data);
}
