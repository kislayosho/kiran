/*
 */
/* 
Copyright 2016 iGenie Ltd
*/ 
package com.kislay.dtd.parser;

import com.kislay.dtd.decl.Comment;
import com.kislay.matra.data.DTDData;
import com.kislay.matra.exception.DTDSyntaxException;

/**
 * Interface for reading comment text.
 *
 * @author Kislay Kumar
 */
public interface CommentReader {

	/**
	 * Read the comment from the data stream.
	 * 
	 * @param data The stream from which to read the comment.
	 * 
	 * @return The comment.
	 * 
	 * @throws DTDSyntaxException If the comment 
	 * 			contains a syntax error. 
	 */
	public Comment readComment(DTDData data) throws DTDSyntaxException;
	
	/**
	 * Checks if there is a comment at the 
	 * current location of the data.
	 * 
	 * @param data The data to be parsed.
	 * 
	 * @return <code>true</code> if the current location
	 * 	of the data has a comment; <code>false</code> otherwise.
	 */
	public boolean isCommentStart(DTDData data);
}
