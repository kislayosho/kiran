/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* 
Copyright 2016 iGenie Ltd
*/ 
package com.kislay.dtd.decl;

/**
 * Interface to hold the ExternalID declaration
 * used for Entity and Notation declarations.
 *
 * @author Kislay Kumar
 */
public interface ExternalID {

	/**
	 * Returns the System Identifier.
	 * 
	 * @see #setSystemIdentifier
	 * 
	 * @return The System Identifier.
	 */
	public String getSystemIdentifier();
	
	/**
	 * Sets the System Identifier.
	 * 
	 * @see #getSystemIdentifier
	 * 
	 * @param systemId The System Identifier.
	 */
	public void setSystemIdentifier(String systemId);
	
	/**
	 * Returns the Public Identifier.
	 * 
	 * @see #setPublicIdentifier
	 * 
	 * @return The Public Identifier.
	 */
	public String getPublicIdentifier();

	/**
	 * Sets the Public Identifier.
	 * 
	 * @see #getPublicIdentifier
	 * @param publicId The Public Identifier.
	 */
	public void setPublicIdentifier(String publicId);

	/**
	 * Returns <code>true</code> if the 
	 * System Identifier is specified.
	 * 
	 * @return <code>true</code> if the 
	 * 		System Identifier is specified
	 * 		<code>false</code> otherwise.
	 */	
	public boolean hasSystemIdentifier();
	
	/**
	 * Returns <code>true</code> if the 
	 * Public Identifier is specified.
	 * 
	 * @return <code>true</code> if the 
	 * 		Public Identifier is specified
	 * 		<code>false</code> otherwise.
	 */	
	public boolean hasPublicIdentifier();
}
