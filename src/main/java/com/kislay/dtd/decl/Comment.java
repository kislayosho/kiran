/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* 
Copyright 2016 iGenie Ltd
*/ 
package com.kislay.dtd.decl;

/**
 * Interface to hold a comment in the dtd.
 *
 * @author Kislay Kumar
 */
public interface Comment {

	/**
	 * Returns the text contained in the 
	 * comment.
	 * 
	 * @see #setCommentText
	 * 
	 * @return The comment text.
	 */
	public String getCommentText();
	
	/**
	 * Sets the comment text.
	 * 
	 * @see #getCommentText
	 * 
	 * @param commentText The comment text.
	 */
	public void setCommentText(String commentText);
}
