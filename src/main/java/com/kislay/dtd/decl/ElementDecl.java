/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* 
Copyright 2016 iGenie Ltd
*/ 
package com.kislay.dtd.decl;

import com.kislay.dtd.decl.elem.ContentSpec;

/**
 * This interface defines an Element Type declaration. 
 * <br/>
 * From the XML Specifications 
 * [Section 3.2 Element Type Declarations] -
 * <br/>
 * The element structure of an XML document may, 
 * for validation purposes, be constrained using 
 * element type and attribute-list declarations. 
 * An element type declaration constrains the 
 * element's content.
 *
 * @author Kislay Kumar
 */
public interface ElementDecl {

	/**
	 * Returns the element name for
	 * this element declaration.
	 * 
	 * @see #setElementName
	 * 
	 * @return The element name.
	 */
	public String getElementName();
	
	/**
	 * Sets the element name for
	 * this element declaration.
	 * 
	 * @see #getElementName
	 * 
	 * @param elemName The element name.
	 */
	public void setElementName(String elemName);
	
	/**
	 * Returns the content spec for this 
	 * element declaration.
	 * 
	 * @see #setContentSpec
	 * 
	 * @return The content spec.
	 */
	public ContentSpec getContentSpec();
	
	/**
	 * Sets the content spec for this 
	 * element declaration.
	 * 
	 * @see #getContentSpec
	 * 
	 * @param contentSpec The content spec.
	 */
	public void setContentSpec(ContentSpec contentSpec);
}
