/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* 
Copyright 2016 iGenie Ltd
*/ 
package com.kislay.dtd.decl;

/**
 * Interface to hold the Notation Declaration.
 *
 * @author Kislay Kumar
 */
public interface Notation extends ExternalID {

	/**
	 * Returns the Notation name.
	 * 
	 * @see #setNotationName
	 * 
	 * @return The Notation name.
	 */
	public String getNotationName();
	
	/**
	 * Sets the Notation name.
	 * 
	 * @see #getNotationName
	 * 
	 * @param notationName The Notation name.
	 */
	public void setNotationName(String notationName);
}
