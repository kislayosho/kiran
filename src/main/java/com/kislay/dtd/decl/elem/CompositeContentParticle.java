/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* 
Copyright 2016 iGenie Ltd
*/ 
package com.kislay.dtd.decl.elem;

import java.util.Collection;

/**
 * Holds a composite CP that holds either choice lists of 
 * content particles, or sequence lists of content particles.
 *
 * @author Kislay Kumar
 */
public interface CompositeContentParticle extends ContentParticle {

	/**
	 * Identifies the type of composition to be choice.
	 */
	public static final short CP_TYPE_CHOICE = 1;

	/**
	 * Identifies the type of composition to be sequence.
	 */
	public static final short CP_TYPE_SEQUENCE = 2;
	
	/**
	 * Returns the type for this Composite CP. The value returned
	 * can be either <code>CP_TYPE_CHOICE</code> if this is a
	 * "choice lists of content particles" or
	 * <code>CP_TYPE_SEQUENCE</code> if this is a
	 * "sequence lists of content particles".
	 * 
	 * @see #setCPType
	 * 
	 * @return <code>CP_TYPE_CHOICE</code> if this is a
	 * "choice lists of content particles" or
	 * <code>CP_TYPE_SEQUENCE</code> if this is a
	 * "sequence lists of content particles".
	 */
	public short getCPType();
	
	/**
	 * Sets the type for this Composite CP.
	 * The value passed can be either 
	 * <code>CP_TYPE_CHOICE</code> if this is a
	 * "choice lists of content particles" or
	 * <code>CP_TYPE_SEQUENCE</code> if this is a
	 * "sequence lists of content particles".
	 * 
	 * @see #getCPType
	 * 
	 * @param type The type for this Composite CP.
	 */
	public void setCPType(short type);
	
	/**
	 * Sets the list of ContentParticle associated
	 * with this CompositeContentParticle.
	 * 
	 * @see #getCPList
	 * 
	 * @param list List of ContentParticle
	 */
	public void setCPList(Collection list);
	
	/**
	 * Returns the list of ContentParticle associated
	 * with this CompositeContentParticle.
	 * 
	 * @see #setCPList
	 * 
	 * @return List of ContentParticle.
	 */
	public Collection getCPList();
}
