/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* 
Copyright 2016 iGenie Ltd
*/ 
package com.kislay.dtd.decl.elem;

/**
 * The ContentParticle. 
 * <p/>
 * From the XML Specifications -
 * <br/>
 * content particles (cps), consist of names, choice lists of content 
 * particles, or sequence lists of content particles.
 * 
 * [48]    cp    ::=    (Name | choice | seq) ('?' | '*' | '+')? 
 *
 * @author Kislay Kumar
 */
public interface ContentParticle {

	/*
	 * From the XML Specifications [Section 3.2.1 Element Content]
	 * 
	 * 	The optional character following a name or list governs 
	 * 	whether the element or the content particles in the list 
	 * 	may occur one or more (+), zero or more (*), or zero or 
	 * 	one times (?). The absence of such an operator means that 
	 * 	the element or content particle must appear exactly once.
	 */

	/**
	 * Constant to denote and optional cardinality.
	 */
	public static final short CARDINALITY_OPTIONAL = 1; // '?'

	/**
	 * Constant to denote and multiple cardinality.
	 */
	public static final short CARDINALITY_MULTIPLE = 2; // '+'

	/**
	 * Constant to denote and optional multiple cardinality.
	 */
	public static final short CARDINALITY_OPTIONAL_MULTIPLE = 3; // '*'

	/**
	 * Constant to denote and single cardinality.
	 */
	public static final short CARDINALITY_ONCE = 4; // ''
	
	/**
	 * Returns the cardinality for this ContentParticle.
	 * <br/>
	 * The cardinality can have one of four values - 
	 * CARDINALITY_OPTIONAL ('?'), CARDINALITY_MULTIPLE ('+') or
	 * CARDINALITY_OPTIONAL_MULTIPLE ('*') or
	 * CARDINALITY_ONCE.
	 * 
	 * @see #setCardinality
	 * 
	 * @return The cardinality for this ContentParticle.
	 */
	public short getCardinality();
	
	/**
	 * Sets the cardinality for this ContentParticle.
	 * <br/>
	 * The cardinality can have one of four values - 
	 * CARDINALITY_OPTIONAL ('?'), CARDINALITY_MULTIPLE ('+') or
	 * CARDINALITY_OPTIONAL_MULTIPLE ('*') or
	 * CARDINALITY_ONCE.
	 * 
	 * @see #getCardinality
	 * 
	 * @param cardinality The cardinality for this ContentParticle.
	 */
	public void setCardinality(short cardinality);
}
