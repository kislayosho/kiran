/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* 
Copyright 2016 iGenie Ltd
*/ 
package com.kislay.dtd.decl.elem;

import java.util.Collection;

/**
 * The ContentModel specifies a ContentSpec
 * where the element contains only child elements. 
 * 
 * This is represented by a collection of 
 * ContentParticle(s), the type - either a
 * choice or sequence and the cardinality of
 * the entire group.
 * <p/>
 * From XML Specifications <br/> 
 * [Section 3.2.1 Element Content] -
 * <br/>
 * [Definition: An element type has <b>element content</b> 
 * when elements of that type must contain only child 
 * elements (no character data), optionally separated 
 * by white space (characters matching the nonterminal 
 * S).][Definition: In this case, the constraint includes 
 * a <b>content model</b>, a simple grammar governing the 
 * allowed types of the child elements and the order in 
 * which they are allowed to appear.] 
 *
 * @author Kislay Kumar
 */
public interface ContentModel extends ContentSpec {

	/**
	 * Sets the element content for this content model.
	 * 
	 * @param CPs The collection of <code>ContentParticle</code>(s) that constitute
	 * 			the element content. It is an error if this parameter is null.
	 * @param type The type of the mix of elements - it can have one of two values
	 * 			CP_TYPE_CHOICE or CP_TYPE_SEQUENCE. If there is only one ContentParticle
	 * 			in this content model, then the type should be CP_TYPE_SEQUENCE.
	 * @param cardinality The cardinality of this content model. The cardinality 
	 * 				can have one of four values - 
	 * 				CARDINALITY_OPTIONAL ('?'), CARDINALITY_MULTIPLE ('+') or
	 * 				CARDINALITY_OPTIONAL_MULTIPLE ('*') or CARDINALITY_ONCE.
	 */
	public void setElementContent(Collection CPs, short type, short cardinality);
}
