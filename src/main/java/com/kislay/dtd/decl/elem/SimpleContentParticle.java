/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* 
Copyright 2016 iGenie Ltd
*/ 
package com.kislay.dtd.decl.elem;

/**
 * This class holds the ContentParticle that contains
 * an Element Name followed by the Cardinality.
 *
 * @author Kislay Kumar
 */
public interface SimpleContentParticle extends ContentParticle {

	/**
	 * Returns the element name that this 
	 * content particle holds.
	 * 
	 * @see #setElementName
	 * 
	 * @return The element name held by this CP.
	 */
	public String getElementName();

	/**
	 * Sets the element name that this 
	 * content particle holds.
	 * 
	 * @see #getElementName
	 * 
	 * @param elemName The element name held by this CP.
	 */	
	public void setElementName(String elemName);
}
