/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* 
Copyright 2016 iGenie Ltd
*/ 
package com.kislay.dtd.decl.elem;

import java.util.Collection;

/**
 * Interface defining a Mixed Content. 
 * <p/>
 * From the XML Specifications
 * <br/>
 * [Definition: An element type has mixed content 
 * when elements of that type may contain character 
 * data, optionally interspersed with child elements.] 
 * In this case, the types of the child elements may 
 * be constrained, but not their order or their 
 * number of occurrences.
 *
 * @author Kislay Kumar
 */
public interface MixedContent extends ContentSpec {

	/**
	 * Returns a Collection of Element names (other than #PCDATA)
	 * that constitute this mixed contentspec. If the 
	 * content spec is "(#PCDATA)" then this method will 
	 * return null.
	 * 
	 * @see #setElementList
	 * 
	 * @return A Collection of String element names that
	 * 		constitute this mixed content.
	 */
	public Collection getElementList();
	
	/**
	 * Sets the collection of element names that constitute
	 * this mixed content.
	 * 
	 * @see #getElementList
	 * 
	 * @param elemList A Collection of String element names that
	 * 		constitute this mixed content.
	 */
	public void setElementList(Collection elemList);
	
	/**
	 * Adds an element name to the list of valid
	 * child elements.
	 * 
	 * @param elemName The element name to add.
	 */
	public void addElement(String elemName);
}
