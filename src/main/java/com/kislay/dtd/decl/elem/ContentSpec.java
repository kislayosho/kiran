/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* 
Copyright 2016 iGenie Ltd
*/ 
package com.kislay.dtd.decl.elem;

/**
 * Interface defining a content specification 
 * specified for an Element in the element
 * declaration.
 *
 * @author Kislay Kumar
 */
public interface ContentSpec {

}
