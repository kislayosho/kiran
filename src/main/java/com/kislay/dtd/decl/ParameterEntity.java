/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* 
Copyright 2016 iGenie Ltd
*/ 
package com.kislay.dtd.decl;

/**
 * Interface to hold the declaration of 
 * a Parameter Entity.
 *
 * @author Kislay Kumar
 */
public interface ParameterEntity extends Entity {

}
