/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* 
Copyright 2016 iGenie Ltd
*/ 
package com.kislay.dtd.decl;

import java.util.Collection;

/**
 * This interface defines an attribute list
 * declaration. 
 * <br/>
 * From the XML Specification
 * [Section 3.3 Attribute-List Declarations] -
 * <br/>
 * [Definition: Attribute-list declarations specify 
 * the name, data type, and default value (if any) 
 * of each attribute associated with a given element 
 * type]
 *
 * @author Kislay Kumar
 */
public interface AttlistDecl {

	/**
	 * Returns the element name associated with this
	 * attribute list declaration.
	 * 
	 * @see #setElementName
	 * 
	 * @return The element name.
	 */
	public String getElementName();
	
	/**
	 * Sets the element name associated with this
	 * attribute list declaration.
	 * 
	 * @see #getElementName
	 * 
	 * @param elemName The element name.
	 */
	public void setElementName(String elemName);
	
	/**
	 * Adds an attribute definition to this
	 * attribute list declaration.
	 * 
	 * @param attDef Attribute definition to add.
	 */
	public void addAttDef(AttDef attDef);
	
	/**
	 * Adds Attribute definitions to this
	 * attribute list declaration.
	 * 
	 * @param attDefs Attribute definitions to add.
	 */
	public void addAttDefs(Collection attDefs);
	
	/**
	 * Returns the attribute definitions
	 * contained within this attribute list
	 * declaration.
	 * 
	 * @return Collection of AttDef objects.
	 */
	public Collection getAttDefs();
}
