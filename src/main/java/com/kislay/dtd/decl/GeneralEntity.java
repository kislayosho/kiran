/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* 
Copyright 2016 iGenie Ltd
*/ 
package com.kislay.dtd.decl;

/**
 * Interface to hold the declaration of 
 * a General Entity.
 *
 * @author Kislay Kumar
 */
public interface GeneralEntity extends Entity {

	/**
	 * Returns the  Notation name, is any, associated
	 * with this Entity.
	 * 
	 * @see #setNotationName
	 * 
	 * @return The Notation name.
	 */
	public String getNotationName();
	
	/**
	 * Sets the  Notation name, is any, associated
	 * with this Entity.
	 * 
	 * @see #getNotationName
	 * 
	 * @param name The Notation name.
	 */	
	public void setNotationName(String name);
}
