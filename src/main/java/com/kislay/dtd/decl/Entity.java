/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* 
Copyright 2016 iGenie Ltd
*/ 
package com.kislay.dtd.decl;

/**
 * Interface defining the Entity Declaration.
 *
 * @author Kislay Kumar
 */
public interface Entity extends ExternalID {

	/**
	 * Returns the name of this entity.
	 * 
	 * @see #setEntityName
	 * 
	 * @return The Entity name.
	 */
	public String getEntityName();
	
	/**
	 * Sets the name for this entity.
	 * 
	 * @see #getEntityName
	 * 
	 * @param name The Entity name.
	 */
	public void setEntityName(String name);
	
	/**
	 * Returns the literal value of this Entity.
	 * 
	 * @see #setLiteralValue
	 * 
	 * @return The Entity's literal value.
	 */
	public String getLiteralValue();
	
	/**
	 * Sets the literal value of this Entity.
	 * 
	 * @see #getLiteralValue
	 * 
	 * @param value The Entity's literal value.
	 */
	public void setLiteralValue(String value);
	
	/**
	 * Returns the replacement text for this Entity.
	 * <br/>
	 * From the XML 1.0 Specification - 
	 * [Definition: The replacement text is the 
	 * content of the entity, after replacement 
	 * of character references and parameter-entity 
	 * references.]
	 * 
	 * @return The replacement text.
	 */
	public String getReplacementText();


	/**
	 * Returns <code>true</code> if this entity is an
	 * internal entity; <code>false</code> otherwise.
	 * <br/>
	 * From the XML 1.0 Specifications - 
	 * [Definition: If the entity definition is an 
	 * EntityValue, the defined entity is called an 
	 * internal entity. There is no separate physical 
	 * storage object, and the content of the entity 
	 * is given in the declaration.] Note that some 
	 * processing of entity and character references in 
	 * the literal entity literalValue may be required to 
	 * produce the correct replacement text
	 * 
	 * @return <code>true</code> if this entity is an
	 * 		internal entity; <code>false</code> otherwise.
	 */
	public boolean isInternalEntity();

	/**
	 * Returns <code>true</code> if this entity is an
	 * external entity; <code>false</code> otherwise.
	 * <br/>
	 * From the XML 1.0 Specifications -
	 * Definition: If the entity is not internal, 
	 * it is an external entity
	 * 
	 * @return <code>true</code> if this entity is an
	 * 		external entity; <code>false</code> otherwise.
	 */
	public boolean isExternalEntity();


	/**
	 * Returns <code>true</code> if this entity is an
	 * parsed entity; <code>false</code> otherwise.
	 * <br/>
	 * From XML 1.0 Specifications -
	 * [Definition: A parsed entity's contents are 
	 * referred to as its replacement text; this text 
	 * is considered an integral part of the document.]
	 * 
	 * @return <code>true</code> if this entity is an
	 * 		parsed entity; <code>false</code> otherwise.
	 */
	public boolean isParsedEntity();

	/**
	 * Returns <code>true</code> if this entity is an
	 * parsed entity; <code>false</code> otherwise.
	 * <br/>
	 * From XML 1.0 Specifications -
	 * [Definition: An unparsed entity is a resource 
	 * whose contents may or may not be text, and if 
	 * text, may be other than XML. Each unparsed 
	 * entity has an associated notation, identified 
	 * by name. Beyond a requirement that an XML 
	 * processor make the identifiers for the entity 
	 * and notation available to the application, 
	 * XML places no constraints on the contents of 
	 * unparsed entities.]
	 * 
	 * @return <code>true</code> if this entity is an
	 * 		parsed entity; <code>false</code> otherwise.
	 */
	public boolean isUnparsedEntity();
	
	/**
	 * Returns <code>true</code> if the Entity is
	 * a General Entity and <code>false</code> otherwise.
	 * 
	 * @return <code>true</code> for General Entity
	 * 		and <code>false</code> otherwise.
	 */
	public boolean isGeneralEntity();
	
	/**
	 * Returns <code>true</code> if the Entity is
	 * a Parameter Entity and <code>false</code> otherwise.
	 * 
	 * @return <code>true</code> for Parameter Entity
	 * 		and <code>false</code> otherwise.
	 */
	public boolean isParameterEntity();
}
