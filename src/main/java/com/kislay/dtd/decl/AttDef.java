/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* 
Copyright 2016 iGenie Ltd
*/ 
package com.kislay.dtd.decl;

import java.util.Collection;

/**
 * Interface to hold the Attribute definition.
 *
 * @author Kislay Kumar
 */
public interface AttDef {
	
	/*
	 * From the XML Specification
	 * 
	 * XML attribute types are of three kinds: 
	 * a string type, a set of tokenized types, 
	 * and enumerated types. The string type may 
	 * take any literal string as a value; the 
	 * tokenized types have varying lexical and 
	 * semantic constraints.
	 * 
	 * [Definition: Enumerated attributes can take 
	 * one of a list of values provided in the 
	 * declaration]. 
	 */
	
	/**
	 * Identifies an Attribute defined as a String 
	 * type. 
	 * This is identified by having a value of
	 * "CDATA" for the AttType. 
	 */
	public static final short TYPE_STRING = 1;
	
	/**
	 * Identifies an Attribute defined as a Tokenized 
	 * type. 
	 * This is identified by AttType having one of 
	 * the following values -
	 * "ID", "IDREF", "IDREFS", "ENTITY", "ENTITIES", 
	 * "NMTOKEN" or "NMTOKENS". 
	 */
	public static final short TYPE_TOKENIZED = 2;
	
	/**
	 * Identifies an Attribute defined as a Enumerated 
	 * type. This includes both Notations and Enumerations.
	 * 
	 * This is identified by having an enumerated list
	 * of values specified for the attribute in the
	 * attribute definition. 
	 */
	public static final short TYPE_ENUMERATED = 3;

	/**
	 * Identifies an Attribute defined as an Enumeration.
	 */
	public static final short TYPE_ENUM_ENUMERATION = 4;
	
	/**
	 * Identifies an Attribute defined as a Notation.
	 */
	public static final short TYPE_ENUM_NOTATION = 5;
	
	/**
	 * Identifies the Tokenized type "ID".
	 */
	public static final short TYPE_TOKENIZED_ID = 6;

	/**
	 * Identifies the Tokenized type "IDREF".
	 */
	public static final short TYPE_TOKENIZED_IDREF = 7;

	/**
	 * Identifies the Tokenized type "IDREFS".
	 */
	public static final short TYPE_TOKENIZED_IDREFS = 8;

	/**
	 * Identifies the Tokenized type "ENTITY".
	 */
	public static final short TYPE_TOKENIZED_ENTITY = 9;

	/**
	 * Identifies the Tokenized type "ENTITIES".
	 */
	public static final short TYPE_TOKENIZED_ENTITIES = 10;

	/**
	 * Identifies the Tokenized type "NMTOKEN".
	 */
	public static final short TYPE_TOKENIZED_NMTOKEN = 11;

	/**
	 * Identifies the Tokenized type "NMTOKENS".
	 */
	public static final short TYPE_TOKENIZED_NMTOKENS = 12;
	
	/**
	 * Identifies an invalid Tokenized type specification.
	 */
	public static final short TYPE_TOKENIZED_INVALID = -1; //invalid type //CR: TODO: revisit this
	
	/**
	 * Identifies an non-Tokenized type attribute.
	 */
	public static final short TYPE_NOT_TOKENIZED = -2; //Not a Tokenized type.
		 
	/**
	 * Identifies a Required attribute. This is recognized in
	 * the attribute definition by the use of the "#REQUIRED"
	 * clause in the DefaultDecl section.
	 */
	public static final short OPTIONALITY_REQUIRED = 1;

	/**
	 * Identifies a Implied attribute. This is recognized in
	 * the attribute definition by the use of the "#IMPLIED"
	 * clause in the DefaultDecl section.
	 */
	public static final short OPTIONALITY_IMPLIED  = 2;

	/**
	 * Identifies a Fixed attribute. This is recognized in
	 * the attribute definition by the use of the "#FIXED"
	 * clause in the DefaultDecl section.
	 */
	public static final short OPTIONALITY_FIXED    = 3;

	/**
	 * Identifies a Required attribute. This is recognized in
	 * the attribute definition by the use of non of the clauses
	 * in the DefaultDecl section, other than the default value.
	 */
	public static final short OPTIONALITY_DEFAULT  = 4;
	
	/**
	 * Returns the name of the attribute.
	 * 
	 * @see #setAttributeName
	 * 
	 * @return The Attribute name.
	 */
	public String getAttributeName();
	
	/**
	 * Set the attribute name for this
	 * attribute definition.
	 * 
	 * @see #getAttributeName
	 * 
	 * @param attName The attribute name.
	 */
	public void setAttributeName(String attName);
	
	/**
	 * Returns the attribute type. Can have 
	 * one of three values - 
	 * <code>TYPE_STRING</code>,
	 * <code>TYPE_TOKENIZED</code> or
	 * <code>TYPE_ENUMERATED</code>.
	 * 
	 * @see #setAttType
	 * 
	 * @return The attribute type.
	 */
	public short getAttType();
	
	/**
	 * Sets the attribute type for this 
	 * attribute definition. It can have one 
	 * of the following values -
	 * TYPE_STRING, TYPE_ENUM_ENUMERATION, 
	 * TYPE_ENUM_NOTATION,
	 * TYPE_TOKENIZED_ID, TYPE_TOKENIZED_IDREF, 
	 * TYPE_TOKENIZED_IDREFS, TYPE_TOKENIZED_ENTITY,
	 * TYPE_TOKENIZED_ENTITIES, TYPE_TOKENIZED_NMTOKEN
	 * or TYPE_TOKENIZED_NMTOKENS.
	 * 
	 * @see #getAttType
	 * 
	 * @param attType The attribute type.
	 */
	public void setAttType(short attType);
	
	/**
	 * Returns <code>true</code> if the attribute
	 * is a String type. This will be the case when
	 * the type of the attribute is declared as "CDATA".
	 * 
	 * @return <code>true</code> if the attribute
	 * is a String type; <code>false</code> otherwise.
	 */
	public boolean isStringType();
	
	/**
	 * Returns <code>true</code> if the attribute
	 * is a Tokenized type. This will be the case when 
	 * the type of the attribute is declared as "ID", 
	 * "IDREF", "IDREFS", "ENTITY", "ENTITIES", "NMTOKEN"
	 * or "NMTOKENS".
	 * 
	 * @return <code>true</code> if the attribute
	 * is a Tokenized type; <code>false</code> otherwise.
	 */
	public boolean isTokenizedType();
	
	/**
	 * Returns <code>true</code> if the attribute
	 * is a Enumerated type.
	 * <br/>
	 * From the XML Specifications -
	 * [Definition: Enumerated attributes can take 
	 * one of a list of values provided in the 
	 * declaration].
	 *  
	 * @return <code>true</code> if the attribute
	 * is a Enumerated type; <code>false</code> otherwise.
	 */
	public boolean isEnumeratedType();

	/**
	 * Returns the type of the Tokenized type for
	 * this attribute.
	 * 
	 * The Tokenized Type can have one of these values -
	 * TYPE_TOKENIZED_ID, TYPE_TOKENIZED_IDREF, 
	 * TYPE_TOKENIZED_IDREFS, TYPE_TOKENIZED_ENTITY,
	 * TYPE_TOKENIZED_ENTITIES, TYPE_TOKENIZED_NMTOKEN
	 * or TYPE_TOKENIZED_NMTOKENS.
	 * 
	 * @return The type of the Tokenized attribute.
	 */
	public short getTokenizedType();
	
	/**
	 * Returns <code>true</code> if the attribute is
	 * of Notation type.
	 * 
	 * @return <code>true</code> if the attribute is a
	 * Notation type; <code>false</code> otherwise.
	 */
	public boolean isNotationType();
	
	/**
	 * Returns <code>true</code> if the attribute is
	 * an Enumeration.
	 * 
	 * @return <code>true</code> if the attribute is
	 * an Enumeration; <code>false</code> otherwise.
	 */
	public boolean isEnumeration();
	
	/**
	 * Returns the possible values defined for an  
	 * Enumerated Type attribute - note that this is
	 * for both Notation as well as Enumerations.
	 * 
	 * @see #setEnumeratedValues
	 * 
	 * @return This list of possible values. A 
	 * 		Collection of String objects.
	 */
	public Collection getEnumeratedValues();
	
	/**
	 * Sets the enumerated values for this attribute
	 * definition.
	 * 
	 * @see #getEnumeratedValues
	 * 
	 * @param enumVals The enumerated values as a 
	 * 		Collection of String objects.
	 */
	public void setEnumeratedValues(Collection enumVals);
	
	/**
	 * Returns the default declartion type for this
	 * attribute. 
	 * 
	 * The default declartion type can have one 
	 * of these values - 
	 * OPTIONALITY_REQUIRED, OPTIONALITY_IMPLIED,
	 * OPTIONALITY_FIXED or OPTIONALITY_DEFAULT.
	 * 
	 * @see #setDefaultDeclType
	 * 
	 * @return The default declaration type.
	 */
	public short getDefaultDeclType();
	
	/**
	 * Set the default declaration type
	 * for this attribute definition.
	 * 
	 * @see #getDefaultDeclType
	 * 
	 * @param defDeclType The default declaration
	 * 			type.
	 */
	public void setDefaultDeclType(short defDeclType);

	/**
	 * Returns <code>true</code> if the attribute
	 * has a fixed or default value.
	 * 
	 * @return <code>true</code> if the attribute
	 * has a fixed or default value; <code>false</code>
	 * otherwise.
	 */
	public boolean hasDefaultValue();
	
	/**
	 * Returns the default value for the attribute -
	 * this will be either the default or fixed value
	 * for the attribute.
	 * 
	 * @see #setDefaultValue
	 * 
	 * @return The default value of the attribute.
	 */
	public String getDefaultValue();
	//CR: TODO: What to do if the default value has some entity reference.
	
	/**
	 * Set the default value for the attribute definition.
	 * 
	 * @see #getDefaultValue
	 * 
	 * @param defValue The default value.
	 */
	public void setDefaultValue(String defValue);
}
