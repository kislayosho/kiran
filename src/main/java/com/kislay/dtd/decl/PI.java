/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* 
Copyright 2016 iGenie Ltd
*/ 
package com.kislay.dtd.decl;

/**
 * Interface to hold a Processing Instruction. 
 * <p/>
 * From the XML Specifications (2nd edition) -
 * <br/>
 * [Definition: Processing instructions (PIs) 
 * allow documents to contain instructions for 
 * applications.]
 *
 * @author Kislay Kumar
 */
public interface PI {

	/**
	 * Returns the PI Target. 
	 * <p/>
	 * From the XML Specification -
	 * <br/>
	 * The PI begins with a target (PITarget) used 
	 * to identify the application to which the 
	 * instruction is directed. The target names 
	 * "XML", "xml", and so on are reserved for 
	 * standardization in this or future versions 
	 * of this specification. The XML Notation 
	 * mechanism may be used for formal declaration 
	 * of PI targets.
	 * 
	 * @see #setPITarget
	 * 
	 * @return The PI Target.
	 */
	public String getPITarget();
	
	/**
	 * Sets the Target for this PI.
	 * 
	 * @see #getPITarget
	 * 
	 * @param target The target for this PI.
	 */
	public void setPITarget(String target);
	
	/**
	 * Returns the character data that constitute
	 * the instructions.
	 * 
	 * @see #setInstructions
	 * 
	 * @return The instructions.
	 */
	public String getInstructions();
	
	/**
	 * Sets the character data that constitute
	 * the instructions.
	 * 
	 * @see #getInstructions
	 * 
	 * @param data The instructions.
	 */
	public void setInstructions(String data);
}

