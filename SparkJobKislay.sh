#!/bin/bash

USAGE="USAGE: <inputFilePath:string:required> <isLocalFile:boolean:required> <fileFormat:string:required > <fileDefination:string:required> <outputPath:string:required> <partitionFormat:string:optional> <errorPath:string:required> "
#contants
HADOOP_BIN="/home/hadoop/hadoop/hadoop-2.6.4/bin"
SPARK_BIN="/home/hadoop/spark/spark-1.6.1-bin-hadoop2.6/bin"


#Variable assignment
inputFilePath=${1:-""}
isLocalFile=${2:-false}
fileFormat=${3:-"xml"}
fileDefination=${4:-""}
outputPath=${5:-""}
partitionFormat=${6:-""}
errorPath=${7:-""}
validatorPath=${8:-""}
schemapath=${9:-""}
validationFile=${10:-""}
rowtag=${11:-""}
enablemysql=${12:-""}
#redirect all 
1>&2 


#Copy local file to HDFS

if $isLocalFile
  then
    $HADOOP_BIN/hadoop fs -copyFromLocal $inputFilePath $hdfsInputPath
	if [ "$?" = "0" ]
	then
	  echo "$(date) :: Successfully copied file from local path :: $inputFilePath to hdfs path :: $hdfsInputPath"  
    else
	  echo "$(date) :: !!!!!!!! Eorror occured !!!!! Copying local file to HDFS failed " 
	exit 1
   fi
fi  
  
#Launch Job
  
 echo " ------------------: Spark Job started :----------------------"   
 echo " ------------------: $(date) :----------------------"
echo "Input file path: $inputFilePath"
echo "File Format: $fileFormat"
echo "File definition $fileDefination"
echo "Output Path $outputPath"
echo "Partition Format $partitionFormat"
echo "Error Path $errorPath"
echo "Validator Path $validatorPath"
echo "Row tag: $rowtag"
echo "Validation File $validationFile"

  
$SPARK_BIN/spark-submit \
--class com.kislay.mainpack.DataProcessorCustom \
--master local[*] \
/home/hadoop/KiranSparkJob-1.0-SNAPSHOT-jar-with-dependencies.jar \
$inputFilePath \
$fileFormat \
$fileDefination \
$outputPath \
$partitionFormat \
$errorPath \
$validatorPath \
$rowtag \
$validationFile \
$enablemysql
